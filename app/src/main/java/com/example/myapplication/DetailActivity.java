package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapter.KoperasiAdapter;
import com.example.myapplication.model.KoperasiModel;
import com.example.myapplication.rest.ApiClient;
import com.example.myapplication.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends AppCompatActivity {
    public TextView mTvId, mTvNamaBarang, mTvJenis, mTvHarga, mTvKeterangan;
    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mTvId = findViewById(R.id.id_koperasi);
        mTvNamaBarang = findViewById(R.id.nama_barang);
        mTvJenis = findViewById(R.id.jenis);
        mTvHarga = findViewById(R.id.harga);
        mTvKeterangan = findViewById(R.id.keterangan); mRecyclerView = (RecyclerView) findViewById(R.id.rv_select_koperasi);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        Bundle b = getIntent().getExtras();
        String str2 = b.getString("id", "null");

        Call<List<KoperasiModel>> KoperasiCall = mApiInterface.postDetailKoperasi(str2);
        KoperasiCall.enqueue(new Callback<List<KoperasiModel>>() {
            @Override
            public void onResponse(Call<List<KoperasiModel>> call, Response<List<KoperasiModel>>
                    response) {
                List<KoperasiModel> KontakList = (List<KoperasiModel>) response.body();
                Log.d("Retrofit Get", "Jumlah data Koperasi: " +
                        String.valueOf(KontakList.size()));
                mAdapter = new KoperasiAdapter(KontakList,getBaseContext());
                mRecyclerView.setAdapter(mAdapter);
                Toast.makeText(DetailActivity.this, "berhasil", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<KoperasiModel>> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
                Toast.makeText(DetailActivity.this, "failed", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intentBack);
        finish();
    }
}
