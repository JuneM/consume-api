package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.rest.ApiClient;
import com.example.myapplication.rest.ApiInterface;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InsertActivity extends AppCompatActivity {

    private EditText mEtNamaBarang,mEtJenis, mEtHarga, mEtKeterangan;
    private Button mBtnSubmit, mBtnCancel;
    ApiInterface mApiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        setUpFindBy();
        setToolListener();
    }
    private void setUpFindBy() {
        mEtNamaBarang = findViewById(R.id.et_nama_barang);
        mEtJenis = findViewById(R.id.et_jenis);
        mEtHarga = findViewById(R.id.et_harga);
        mEtKeterangan = findViewById(R.id.et_keterangan);
        mBtnSubmit = findViewById(R.id.btn_submit);
        mBtnCancel = findViewById(R.id.btn_cancel);
    }

    private void setToolListener() {
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intentBack);
                finish();
            }
        });

        mBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<ResponseBody> postKontakCall = mApiInterface.postInsertKoperasi(mEtNamaBarang.getText().toString(),
                        mEtJenis.getText().toString(), mEtHarga.getText().toString(), mEtKeterangan.getText().toString());
                postKontakCall.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        ResponseBody result = response.body();
                        Toast.makeText(InsertActivity.this, ""+result, Toast.LENGTH_SHORT).show();
                        Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intentBack);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intentBack);
        finish();
    }
}
