package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.adapter.KoperasiAdapter;
import com.example.myapplication.model.KoperasiModel;
import com.example.myapplication.rest.ApiClient;
import com.example.myapplication.rest.ApiInterface;
import com.getbase.floatingactionbutton.FloatingActionButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ApiInterface mApiInterface;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton mFabAdd;
    public static MainActivity ma;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFabAdd = findViewById(R.id.fab_add);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_select_koperasi);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ma=this;
        refresh();
        mFabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentAdd = new Intent(getApplicationContext(), InsertActivity.class);
                startActivity(intentAdd);
                finish();
            }
        });
    }

    public void refresh() {
        Call<List<KoperasiModel>> KoperasiCall = mApiInterface.getKoperasiList();
        KoperasiCall.enqueue(new Callback<List<KoperasiModel>>() {
            @Override
            public void onResponse(Call<List<KoperasiModel>> call, Response<List<KoperasiModel>>
                    response) {
                List<KoperasiModel> KontakList = (List<KoperasiModel>) response.body();
                Log.d("Retrofit Get", "Jumlah data Koperasi: " +
                        String.valueOf(KontakList.size()));
                mAdapter = new KoperasiAdapter(KontakList,ma);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<List<KoperasiModel>> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }
}
