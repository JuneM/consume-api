package com.example.myapplication.model;

import com.google.gson.annotations.SerializedName;

public class KoperasiModel {
    @SerializedName("id")
    private String id;
    @SerializedName("nama_barang")
    private String namaBarang;
    @SerializedName("jenis")
    private String jenis;
    @SerializedName("harga")
    private String harga;
    @SerializedName("keterangan")
    private String keterangan;

    public KoperasiModel(){}

    public KoperasiModel(String id, String namaBarang, String jenis, String harga, String keterangan) {
        this.id = id;
        this.namaBarang = namaBarang;
        this.jenis = jenis;
        this.harga = harga;
        this.keterangan = keterangan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public void setNamaBarang(String namaBarang) {
        this.namaBarang = namaBarang;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
