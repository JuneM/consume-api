package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.DetailActivity;
import com.example.myapplication.R;
import com.example.myapplication.model.KoperasiModel;

import java.util.List;

public class KoperasiAdapter extends RecyclerView.Adapter<KoperasiAdapter.KoperasiViewHolder> {

    private List<KoperasiModel> mKoperasiList;
    private Context mContext;

    public KoperasiAdapter(List<KoperasiModel> mKoperasiList, Context mContext) {
        this.mKoperasiList = mKoperasiList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public KoperasiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_list_koperasi, parent, false);
        KoperasiViewHolder mViewHolder = new KoperasiViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull KoperasiViewHolder holder, int position) {
        final KoperasiModel modelList = mKoperasiList.get(position);
        holder.mTvId.setText(modelList.getId());
        holder.mTvNamaBarang.setText(modelList.getNamaBarang());
        holder.mTvJenis.setText(modelList.getJenis());
        holder.mTvHarga.setText(modelList.getHarga());
        holder.mTvKeterangan.setText(modelList.getKeterangan());
        holder.mRlKoperasi.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent intentDetail = new Intent(mContext, DetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", modelList.getId());
                intentDetail.putExtras(bundle);
                mContext.startActivity(intentDetail);
                ((Activity)mContext).finish();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mKoperasiList.size();
    }

    public class KoperasiViewHolder extends RecyclerView.ViewHolder {
        public TextView mTvId, mTvNamaBarang, mTvJenis, mTvHarga, mTvKeterangan;
        private RelativeLayout mRlKoperasi;
        public KoperasiViewHolder(@NonNull View itemView) {
            super(itemView);
            mTvId = itemView.findViewById(R.id.id_koperasi);
            mTvNamaBarang = itemView.findViewById(R.id.nama_barang);
            mTvJenis = itemView.findViewById(R.id.jenis);
            mTvHarga = itemView.findViewById(R.id.harga);
            mTvKeterangan = itemView.findViewById(R.id.keterangan);
            mRlKoperasi = itemView.findViewById(R.id.rl_koperasi);
        }
    }
}
