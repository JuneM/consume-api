package com.example.myapplication.rest;

import com.example.myapplication.model.KoperasiModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiInterface {

    @GET("select_koperasi")
    Call<List<KoperasiModel>> getKoperasiList();

    @FormUrlEncoded
    @POST("insert_koperasi")
    Call<ResponseBody> postInsertKoperasi(@Field("nama_barang") String nama_barang,
                                          @Field("jenis") String jenis,
                                          @Field("harga") String harga,
                                          @Field("keterangan") String keterangan);
    @FormUrlEncoded
    @POST("get_koperasi")
    Call<List<KoperasiModel>> postDetailKoperasi(@Field("id") String id);
    /*@FormUrlEncoded
    @HTTP(method = "DELETE", path = "kontak", hasBody = true)
    Call<PostPutDelKontak> deleteKontak(@Field("id") String id);*/

}
